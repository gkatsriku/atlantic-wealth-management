<div id="wrapper-footer">   
	<div id="footer-fluid">
      <div id="footer">
        <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third']): ?> 
          <div id="footer-area" class="clearfix">
            <?php if ($page['footer_first']): ?>
            <div class="column"><?php print render($page['footer_first']); ?></div>
            <?php endif; ?>
            <?php if ($page['footer_second']): ?>
            <div class="column"><?php print render($page['footer_second']); ?></div>
            <?php endif; ?>
            <?php if ($page['footer_third']): ?>
            <div class="column"><?php print render($page['footer_third']); ?></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
          
        <div id="copyright">
         <p class="copyright"><?php print t('Copyright'); ?> &copy; <?php echo date("Y"); ?>, <?php print $site_name; ?> </p>
        <div class="clear"></div>
        </div>
      </div>
      </div>
    </div>
</div>