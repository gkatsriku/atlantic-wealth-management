<?
	function render_markup($content)
	{
		return $content["body"][0]["#markup"];
	}

	function render_main_menu($content)
	{
		$output = "<div id=\"navigation\">\n";
		foreach ($content as $link)
		{
			$output .= "\t<a href=\"/" . $link["link_path"] . "\">" . $link["link_title"] . "</a>\n";
		}
		$output .= "</div>";
		return $output;
	}
?>