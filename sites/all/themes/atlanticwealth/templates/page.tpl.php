<?php include_once('header.tpl.php');?>
<div id="wrapper">
<div id="container">

    <?php if ($is_front): ?>
      <?php if (theme_get_setting('slideshow_display', 'atlanticwealth')): ?>
        <!-- Slides -->
  <?php
  $slide1_head = check_plain(theme_get_setting('slide1_head','atlanticwealth'));   $slide1_desc = check_markup(theme_get_setting('slide1_desc','atlanticwealth'), 'full_html'); $slide1_url = check_plain(theme_get_setting('slide1_url','atlanticwealth'));
  $slide2_head = check_plain(theme_get_setting('slide2_head','atlanticwealth'));   $slide2_desc = check_markup(theme_get_setting('slide2_desc','atlanticwealth'), 'full_html'); $slide2_url = check_plain(theme_get_setting('slide2_url','atlanticwealth'));
  $slide3_head = check_plain(theme_get_setting('slide3_head','atlanticwealth'));   $slide3_desc = check_markup(theme_get_setting('slide3_desc','atlanticwealth'), 'full_html'); $slide3_url = check_plain(theme_get_setting('slide3_url','atlanticwealth'));
  ?>
  <!--
    <section id="slider">
    <ul class="slides">
      <li>
        <article class="post">
        <div class="entry-container">
          <header class="entry-header">
            <h2 class="entry-title"><a href="<?php //print url($slide1_url); ?>"><?php //print $slide1_head; ?></a></h2>
          </header><!-- .entry-header -->
        <!--   <div class="entry-summary">
                <?php //print $slide1_desc; ?>
          </div><!-- .entry-summary -->
      <!--     <div class="clear"></div>
        </div><!-- .entry-container -->
     <!--        <a href="<?php //print url($slide1_url); ?>">
            <img src="<?php //print base_path() . drupal_get_path('theme', 'atlanticwealth') . '/images/slide-image-1.jpg'; ?>" class="slide-image" /></a>
         <div class="clear"></div>
        </article>
      </li>
      
      <li>
        <article class="post">
        <div class="entry-container">
          <header class="entry-header">
            <h2 class="entry-title"><a href="<?php //print url($slide2_url); ?>"><?php //print $slide2_head; ?></a></h2>
          </header><!-- .entry-header -->
     <!--      <div class="entry-summary">
                <?php //print $slide2_desc; ?>
          </div><!-- .entry-summary -->
    <!--       <div class="clear"></div>
        </div><!-- .entry-container -->
     <!--        <a href="<?php //print url($slide2_url); ?>">
            <img src="<?php //print base_path() . drupal_get_path('theme', 'atlanticwealth') . '/images/slide-image-2.jpg'; ?>" class="slide-image" /></a>
         <div class="clear"></div>
        </article>
      </li>

      <li>
        <article class="post">
        <div class="entry-container">
          <header class="entry-header">
            <h2 class="entry-title"><a href="<?php //print url($slide3_url); ?>"><?php //print $slide3_head; ?></a></h2>
          </header><!-- .entry-header -->
      <!--     <div class="entry-summary">
                <?php //print $slide3_desc; ?>
          </div><!-- .entry-summary -->
    <!--       <div class="clear"></div>
        </div><!-- .entry-container -->
   <!--          <a href="<?php //print url($slide3_url); ?>">
            <img src="<?php //print base_path() . drupal_get_path('theme', 'atlanticwealth') . '/images/slide-image-3.jpg'; ?>" class="slide-image" /></a>
         <div class="clear"></div>
        </article>
      </li>
    </ul>
    </section>  
    -->
       <?php endif; ?>
    <?php endif; ?>
  
  
  

    <div class="content-sidebar-wrap">

    <div id="content">
      <?php if (theme_get_setting('breadcrumbs', 'atlanticwealth')): ?><div id="breadcrumbs"><?php if ($breadcrumb): print $breadcrumb; endif;?></div><?php endif; ?>
      <section id="post-content" role="main">
        <?php print $messages; ?>
        <?php if ($page['content_top']): ?><div id="content_top"><?php print render($page['content_top']); ?></div><?php endif; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <?php print render($page['content']); ?>
      </section> <!-- /#main -->
    </div>
  
    <?php if ($page['sidebar_first']): ?>
      <aside id="sidebar-first" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>
  
    </div>

    <?php if ($page['sidebar_second']): ?>
      <aside id="sidebar-second" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>
    
    
      <div class="clear"></div>
      <?php if ($page['footer']): ?>
       <div id="foot">
         <?php //print render($page['footer']) ?>
       </div>
       <?php endif; ?>
      </div> 
    </div>
    
<?php include_once('footer.tpl.php');?>