<!DOCTYPE html>
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lte IE 8]>
<style type='text/css'>
	#main-menu ul {
	  float:right;
	  padding:0;
	  margin:0;
	  list-style:none;
	  /*font-size:80%;*/
	  font-size:14px;
	}
	#main-menu li {
	  position:relative;
	  float:left;
	  padding:0;
	  margin:0;
	  line-height:75px;
	  border-right:1px dotted #c2b64d;
	}
	#main-menu {
	  right:0px;
	  top:0px;
	  position:absolute;
	  min-height:123px;
	  width:62%;
	}
    #main-menu a, 
	 #main-menu li > a {
	  color:#fff;
	  text-decoration:none;
	  text-transform:uppercase;
	  padding:40px 20px 0 20px;

	}
	
	#main-menu a:hover, 
	 #main-menu li > a.active {
	  background:#c2b64d;
	  color:#023e7a;
	  text-decoration:none;
	  text-transform:uppercase;
	  padding:40px 20px 0 20px;

	}
</style>
<![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38679726-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>