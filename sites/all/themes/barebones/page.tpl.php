<div id="header">
	<?
		print render_main_menu(menu_load_links("main-menu"));
		if($page["header"])
		{
			print render($page["header"]);
		}
	?>
</div>
<div id="content">
	<?
		if($title)
		{
			echo "<h1 class=\"title\" id=\"page-title\">$title</h1>";
		}
		echo render($title_suffix);
	    if($tabs)
	    {
	    	echo "<div class=\"tabs\">" . render($tabs) . "</div>";
	    }
		if($page["content"])
		{
			print render($page["content"]);
		}
	?>
</div>
<div id="footer">
	<?
		if($page["footer"])
		{
			print render($page["footer"]);
		}
	?>
</div>