<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?=$head_title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Big 8 Water">
    <meta name="author" content="Kris McCann">
    <link href="<?=$directory?>/images/favicon.ico" rel="icon" type="image/x-icon" />
    <?=$styles;?>
    <?=$scripts;?>
  </head>
  <body class="<?php print $classes; ?>">
    <?if($is_admin){print $page_top;}?>
    <?=$page;?>
    <?if($is_admin){print $page_bottom;}?>
    <script type="text/javascript">
      head.ready
      (
        function()
        {
          // load scripts
        }
      );
    </script>
  </body>
</html>